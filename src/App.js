import React from 'react'
import PropTypes from 'prop-types'
import { BrowserRouter as Router } from "react-router-dom";
import Routes from './route/router'
import { connect } from 'react-redux'
import { changeAuthStatus } from './redux/actions'

class App extends React.Component {

  componentDidMount() {
    // Тут менять состояние приложения, авторизирован пользователь или нет
    // this.props.onChangeAuth(true)
  }

  render() {
    const { isAuth } = this.props
    return (
      <Router>
        <Routes isAuth={isAuth}/>
      </Router>
    )
  }
}

App.propTypes = {
  isAuth: PropTypes.bool.isRequired,
  onChangeAuth: PropTypes.func.isRequired,
}

const mapStateToProps = state => {
  return {
    isAuth: state.isAuthenticate
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onChangeAuth: status => {
      dispatch(changeAuthStatus(status))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
