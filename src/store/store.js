import { createStore } from 'redux'
import MainReducer from '../redux/mainReducer'

const store = createStore(MainReducer)

export default store