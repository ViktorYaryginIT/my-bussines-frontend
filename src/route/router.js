import React from 'react'
import { Switch, Route, Redirect } from "react-router-dom";

import Sign from "../views/forms/login"
import Register from "../views/forms/register"
import Recovery from "../views/forms/recovery"
import NewPassord from "../views/forms/newpass"
import Cabinet from "../views/Cabinet"

function Routes ({ isAuth }) {
    console.log(isAuth)
    if (!isAuth) {
        return (
            <Switch>
                <Route exact path="/sign">
                    <Sign />
                </Route>
                <Route exact path="/register">
                    <Register />
                </Route>
                <Route exact path="/recovery">
                    <Recovery />
                </Route>
                <Route exact path="/new-password">
                    <NewPassord />
                </Route>
                <Redirect to={{ pathname: "/sign" }} />
            </Switch>
        )
    } else {
        return (
            <Cabinet />
        )
    }
}

export default Routes