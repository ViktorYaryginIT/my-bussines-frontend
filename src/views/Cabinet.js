import React from 'react'
import { Switch, Route, Link, Redirect } from "react-router-dom";

import Orders from './components/Orders'
import Products from './components/Products'
import Sources from './components/Sources'

class Cabinet extends React.Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    render() {
        return (
            <div>
                <div id="page-loader" className="fade show d-none">
                    <span className="spinner"/>
                </div>
                <div id="page-container" className="fade page-sidebar-fixed page-header-fixed show">
                    <div id="header" className="header navbar-default">
                        <div className="navbar-header">
                            <div className="navbar-brand">
                                <span className="navbar-logo"/>
                                MyBussines
                            </div>
                        </div>
                        
                        <ul className="navbar-nav navbar-right">
                            <li className="dropdown">
                                <a href="#" data-toggle="dropdown" className="dropdown-toggle f-s-14">
                                    <i className="fa fa-bell"/>
                                    <span className="label">5</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div id="sidebar" className="sidebar">
                        <div className="slimScrollDiv">
                            <ul className="nav">
                                <li className="nav-profile">
                                    <div className="cover with-shadow"/>
                                    <div className="image">
                                        <img src="./img/user-13.jpg" alt=""/>
                                    </div>
                                    <div className="info">
                                        Sean Ngu
                                        <small>Front end developer</small>
                                    </div>
                                </li>
                            </ul>
                            <ul className="nav">
                                <li>
                                    <Link to="/orders">
                                        <i className="fa fa-th-large"/>
                                        <span>Мои заказы</span>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/products">
                                        <i className="fa fa-th-large"/>
                                        <span>Моя продукция</span>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/sourse">
                                        <i className="fa fa-th-large"/>
                                        <span>Расходники</span>
                                    </Link>
                                </li>
                                <li>
                                    <a href="javascript:;" className="sidebar-minify-btn" data-click="sidebar-minify">
                                        <i className="fa fa-angle-double-left"/>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="sidebar-bg"/>
                    <div className="content">
                    <Switch>
                        <Route exact path="/orders">
                            <Orders />
                        </Route>
                        <Route exact path="/products">
                            <Products />
                        </Route>
                        <Route exact path="/sourse">
                            <Sources />
                        </Route>
                        <Redirect to={{ pathname: "/orders" }} />
                    </Switch>
                    </div>
                    <a href="javascript:;" className="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
                        <i className="fa fa-angle-up"/>
                    </a>
                </div>     
            </div>
        )
        
    }
}


export default Cabinet