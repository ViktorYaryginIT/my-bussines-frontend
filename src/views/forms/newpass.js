import React from 'react'

class NewPassForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    render() {
        return (
            <div>
                <h2 className="mb-1 text-center title">Создание нового пароля</h2>
                <h6 className="mb-4 text-center title-text">Пожалуйста, придумайте новый пароль</h6>
                <form>
                    <div className="form-group">
                        <input type="password" className="form-control" id="exampleInputNewPassword" placeholder="Новый пароль"/>
                    </div>
                    <div className="form-group">
                        <input type="password" className="form-control" id="exampleRepeatNewPassword" placeholder="Повторите пароль"/>
                    </div>
                    <button type="submit" className="btn btn-primary col-md-12 button-text">Создать</button>      
                </form>
                
            </div>
        )
    }
}

export default NewPassForm 