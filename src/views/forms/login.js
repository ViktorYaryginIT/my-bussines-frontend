import React from 'react'

class LoginForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    render() {
        return (
            <div>
                <h2 className="mb-1 text-center title">Войти в аккаунт</h2>
                <h6 className="mb-4 text-center title-text">Пожалуйста, войдите в свой аккаунт</h6>
                <form>
                    <div className="form-group">
                        <input type="email" class="form-control" id="exampleFormControlInputLogin" placeholder="Email"/>
                    </div>
                    
                    <div className="form-group">
                        <input type="password" className="form-control" id="exampleInputPasswordLogin" placeholder="Пароль"/>
                    </div>
                    
                    <button type="submit" className="btn btn-primary col-md-12 button-text">Войти в аккаунт</button>      
                </form>
                <p class="text-center link"><a href="#">Зарегистрироваться</a></p>
                <p class="text-center link"><a href="#">Забыли пароль?</a></p>
            </div>
        )
    }
}

export default LoginForm 