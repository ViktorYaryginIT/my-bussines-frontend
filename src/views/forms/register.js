import React from 'react';
import { validateInput } from '../../utils/validateInput'

class RegisterForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            name: '',
            password: '',
            passwordConfirm: '',
            errors: {},
            loading: false
        }

        this.isValid = this.isValid.bind(this)
        this.onSubmit = this.onSubmit.bind(this)
        this.setInputValue = this.setInputValue.bind(this)
    }

    isValid() {
        const { errors, isValid } = validateInput(this.state)
        if (!isValid) {
            this.setState({ errors })
        }
        return isValid
    }

    setInputValue(e) {
        this.setState({ [e.target.name]: e.target.value })
    }

    onSubmit(e) {
        e.preventDefault()

        if (!this.isValid()) {
            return
        }

        this.setState({ loading: true })
        
        setTimeout(() => {
            this.setState({ loading: false })
        }, 3000)
    }

    render() {
        return (
            <div>
                <h2 className="mb-1 text-center title">Регистрация</h2>
                <h6 className="mb-4 text-center title-text">Для входа в чат, Вам нужно зарегистрироваться</h6>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <input
                            type="text"
                            name="email"
                            className={`form-control ${this.state.errors.email ? 'is-invalid' : ''}`}
                            placeholder="Email"
                            value={this.state.email}
                            onChange={this.setInputValue} 
                            />
                        <div class="invalid-feedback">
                            {this.state.errors.email}
                        </div>
                    </div>
                    <div className="form-group">
                        <input
                            className={`form-control ${this.state.errors.name ? 'is-invalid' : ''}`}
                            placeholder="Ваше имя"
                            type="text"
                            name="name"
                            value={this.state.name}
                            onChange={this.setInputValue}
                        />
                        <div class="invalid-feedback">
                            {this.state.errors.name}
                        </div>
                    </div>
                    <div className="form-group">
                        <input
                            type="password"
                            className={`form-control ${this.state.errors.password ? 'is-invalid' : ''}`}
                            placeholder="Пароль"
                            name="password"
                            value={this.state.password}
                            onChange={this.setInputValue}
                        />
                        <div class="invalid-feedback">
                            {this.state.errors.password}
                        </div>
                    </div>
                    <div className="form-group">
                        <input
                            type="password"
                            name="passwordConfirm"
                            className={`form-control ${this.state.errors.passwordConfirm ? 'is-invalid' : ''}`}
                            placeholder="Повторите пароль"
                            value={this.state.passwordConfirm}
                            onChange={this.setInputValue}
                        />
                        <div class="invalid-feedback">
                            {this.state.errors.passwordConfirm}
                        </div>
                    </div>
                    <button
                        type="submit"
                        disabled={this.state.loading}
                        className="btn btn-primary col-md-12 button-text">
                            Зарегистрироваться
                        </button>      
                </form>
                <p class="text-center link"><a href="#">Войти в аккаунт</a></p>
            </div>
        )
    }
}

export default RegisterForm