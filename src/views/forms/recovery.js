import React from 'react'

class RecoveryForm extends React.Component {
    constructor(props) {
        super(props)

        this.state = {}
    }

    render() {
        return (
            <div>
                <h2 className="mb-1 text-center title">Восстановление пароля</h2>
                <h6 className="mb-4 text-center title-text">Пожалуйста, введите Ваш Email</h6>
                <form>
                     <div className="form-group">
                        <input type="email" class="form-control" id="exampleFormControlInputForRecovery" placeholder="Email"/>
                    </div>
                    <button type="submit" className="btn btn-primary col-md-12 button-text">Отправить</button>      
                </form>
                
            </div>
        )
    }
}

export default RecoveryForm