import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import App from './App'
import store from './store/store'
import './styles/bootstrap.min.css'
import './styles/all.min.css'
import './styles/index.css'
import './styles/app.css'

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
