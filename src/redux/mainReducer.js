const initialState = {
    isAuthenticate: false
}

function MainReducer( state=initialState, action ) {
    switch(action.type) {
        case 'CHANGE_AUTHENTICATE':
            return {...state, isAuthenticate: action.payload}
        default:
            return state
    }
}

export default MainReducer