import {CHANGE_AUTHENTICATE} from './actionTypes'

export const changeAuthStatus = status => {
    return {
        type: CHANGE_AUTHENTICATE,
        payload: status
    }
}