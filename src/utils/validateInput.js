export const validateInput = input => {
    let errors = {}
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

    if (input.name === '') {
        errors.name = 'Поле обязательно для заполнения!'
    }

    if (input.email === '') {
        errors.email = 'Поле обязательно для заполнения!'
    } else if (!re.test(input.email)) {
        errors.email = 'Введите корректный адрес!'
    }
    
    if (input.password === '') {
        errors.password = 'Поле обязательно для заполнения!'
    }
    
    if (input.passwordConfirm === '') {
        errors.passwordConfirm = 'Поле обязательно для заполнения!'
    } else if (input.passwordConfirm !== input.password) {
        errors.passwordConfirm = 'Пароли не совпадают!'
    }

    return {
        errors,
        isValid: !Object.keys(errors).length
    }
}